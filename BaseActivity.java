package com.example.mayankagarwal.thetask;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Aditya on 03-Aug-18.
 */
public class BaseActivity extends AppCompatActivity {
    private ProgressDialog pDialog;
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialog = new ProgressDialog(BaseActivity.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");

    }

    public void showDialog() {
        if (!isFinishing() && pDialog != null) {
            pDialog.show();
        }
    }

    public void dismissDialog() {
        if (isFinishing()) {
            return;
        }
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}